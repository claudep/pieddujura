from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.urls import include, path
from django.views.static import serve

from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('captcha/', include('captcha.urls')),
    path('summernote/', include('django_summernote.urls')),

    path('', views.HomeView.as_view(), name='home'),
    path('contact/', views.ContactView.as_view(), {'slug': 'contact'}, name='contact'),
    path('agenda/', views.AgendaView.as_view(), {'slug': 'agenda'}, name='agenda'),
    path('evenement/<int:target>/', views.EventView.as_view(), name='event'),

    path('galerie/', views.GalleryListView.as_view(), name='galleries'),
    path('galerie/edit/', login_required(views.PageEditView.as_view()), kwargs={'slug': 'galerie'}),
    path('galerie/<slug:slug>/', views.GalleryDetailView.as_view(), name='gallery'),

    path('<slug:slug>/', views.PageView.as_view(), name='page'),
    path('<slug:slug>/edit/', login_required(views.PageEditView.as_view()), name='page_edit'),
]


if settings.DEBUG:
    urlpatterns += [
        path('media/<path:path>', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
