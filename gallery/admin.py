from django.contrib import admin
from django.db.models import Count

from .models import Gallery, Photo


class PhotoInline(admin.TabularInline):
    model = Photo


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'published', 'photo_count')
    list_editable = ('published',)
    prepopulated_fields = {'slug': ('title',)}
    inlines = [PhotoInline]

    def photo_count(self, obj):
        return obj.photo_count
    photo_count.short_description = "Nombre de photos"

    def get_queryset(self, request):
        """
        Add number of photos to each gallery.
        """
        qs = super().get_queryset(request)
        return qs.annotate(photo_count=Count('photos'))
