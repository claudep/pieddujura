from django.db import models


class Gallery(models.Model):
    title = models.CharField("Titre", max_length=255)
    slug = models.SlugField("Raccourci", max_length=255, unique=True)
    description = models.TextField()
    published = models.BooleanField("Publié", default=False)

    class Meta:
        verbose_name = "Galerie"

    def __str__(self):
        return self.title


class Photo(models.Model):
    created = models.DateTimeField("Date de création", editable=False, auto_now_add=True)
    gallery = models.ForeignKey(Gallery, related_name='photos', verbose_name="Galerie",
        on_delete=models.CASCADE)
    image = models.ImageField(upload_to='photos/%Y/%m/%d')
    title = models.CharField("Titre", max_length=255, blank=True)

    class Meta:
        ordering = ('created',)
