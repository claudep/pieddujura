from gallery.models import Gallery
from .models import Page


def pages(request):
    return {'pages': Page.objects.all(), 'galleries': Gallery.objects.filter(published=True)}
