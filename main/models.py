from datetime import date

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.db import models
from django.urls import reverse
from django.utils import dateformat

from mptt.models import MPTTModel, TreeForeignKey


class Attachment(models.Model):
    upfile = models.FileField("Fichier", upload_to='fichiers')
    title = models.CharField("Titre", max_length=250)
    uploaded = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(default=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    class Meta:
        verbose_name = 'Fichier joint'
        verbose_name_plural = 'Fichiers joints'

    def __str__(self):
        return "%s (%s)" % (self.title, self.upfile)


class Page(MPTTModel):
    slug = models.SlugField()
    title = models.CharField(max_length=150)
    menu_title = models.CharField(max_length=100, blank=True,
        help_text="Titre court de la page pour les menus")
    content = models.TextField()
    parent = TreeForeignKey(
        'self', null=True, blank=True, related_name='children',
        on_delete=models.SET_NULL, db_index=True
    )
    weight = models.IntegerField("Poids", default=0)
    attachments = GenericRelation(Attachment)

    class Meta:
        ordering = ('weight',)

    class MPTTMeta:
        order_insertion_by = ['title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page', kwargs={'slug': self.slug})

    def get_menu_title(self):
        return self.menu_title or self.title


class FutureManager(models.Manager):
    """Future published events."""
    def get_queryset(self):
        return super().get_queryset().filter(pub_date__lte=date.today()
            ).filter(models.Q(end_date__isnull=False, end_date__gte=date.today()) |
                     models.Q(end_date__isnull=True, start_date__gte=date.today()))


class PassedManager(models.Manager):
    """Passed published events."""
    def get_queryset(self):
        return super().get_queryset().filter(pub_date__lte=date.today()
            ).filter(models.Q(end_date__isnull=False, end_date__lt=date.today()) |
                     models.Q(end_date__isnull=True, start_date__lt=date.today()))


class Event(models.Model):
    EVENT_CHOICES = (
        ('cafe', 'Café-Conférence'),
        ('autre', 'Autre'),
    )
    DATE_FORMAT = 'l j F Y'  # mardi 3 avril 2015
    DATE_FORMAT_SHORT = 'j.n.Y'  # 4.4.2015
    TIME_FORMAT = 'H:i'  # 09:30

    pub_date = models.DateField("Date de publication",
        help_text="Si la date est dans le futur, la nouvelle ne sera publiée qu'à cette date")
    start_date = models.DateField("Date début")
    start_hour = models.TimeField("Heure début", blank=True, null=True)
    end_date = models.DateField("Date fin", blank=True, null=True)
    end_hour = models.TimeField("Heure fin", blank=True, null=True)
    title = models.CharField("Titre", max_length=255)
    description = models.TextField("Description", blank=True)
    event_type = models.CharField("Type", max_length=20, choices=EVENT_CHOICES)

    objects = models.Manager()
    published = FutureManager()
    passed = PassedManager()

    class Meta:
        verbose_name = 'Événement'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('event', args=[self.pk])

    @property
    def is_passed(self):
        return (
            (self.end_date is not None and self.end_date < date.today()) or
            (self.end_date is None and self.start_date < date.today())
        )

    def date_long(self, hour=False):
        content = ''
        if self.end_date:
            content = "du %s" % dateformat.format(self.start_date, self.DATE_FORMAT)
            if hour and self.start_hour:
                content += " (%s)" % dateformat.time_format(self.start_hour, self.TIME_FORMAT)
            content += " au %s" % dateformat.format(self.end_date, self.DATE_FORMAT)
            if hour and self.end_hour:
                content += " (%s)" % dateformat.time_format(self.end_hour, self.TIME_FORMAT)
        else:
            content = dateformat.format(self.start_date, self.DATE_FORMAT)
            if hour and self.end_hour and self.start_hour:
                content += " (%s - %s)" % (
                    dateformat.time_format(self.start_hour, self.TIME_FORMAT),
                    dateformat.time_format(self.end_hour, self.TIME_FORMAT))
            elif hour and self.start_hour:
                content += " (%s)" % dateformat.time_format(self.start_hour, self.TIME_FORMAT)
        return content

class Contact(models.Model):
    first_name = models.CharField("Prénom", max_length=50)
    last_name = models.CharField("Nom", max_length=50)
    email = models.EmailField("Courriel", max_length=100)

    def __str__(self):
        return " ".join([self.first_name, self.last_name])
