from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import FormView, TemplateView, UpdateView

from gallery.models import Gallery
from .forms import ContactForm, PageForm
from .models import Event, Page


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context['next_event'] = Event.published.order_by('start_date').first()
        return context


class PageMixin:
    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        page = get_object_or_404(Page, slug=self.kwargs['slug'])
        context.update({
            'title': page.title,
            'page': page,
            'next_event': Event.published.order_by('start_date').first(),
            'can_edit': self.request.user.is_authenticated,
            'attachments': page.attachments.filter(visible=True),
        })
        if page.slug == settings.LIST_PASSED_EVENTS_ON:
            context['passed_events'] = Event.passed.order_by('-start_date')
        return context


class PageView(PageMixin, TemplateView):
    template_name = 'page.html'


class PageEditView(UpdateView):
    template_name = 'page_edit.html'
    model = Page
    form_class = PageForm


class GalleryListView(TemplateView):
    template_name = 'page_galleries.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        page = get_object_or_404(Page, slug='galerie')
        context.update({
            'page': get_object_or_404(Page, slug='galerie'),
            'main_page': page,
            'galleries': Gallery.objects.filter(published=True),
        })
        return context


class GalleryDetailView(TemplateView):
    template_name = 'page_gallery.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        page = get_object_or_404(Page, slug='galerie')
        context.update({
            'page': page,
            'main_page': page,
            'gallery': get_object_or_404(Gallery.objects.prefetch_related('photos'),
                                         slug=self.kwargs['slug']),
        })
        return context


class AgendaView(PageMixin, TemplateView):
    template_name = 'page_agenda.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'events': Event.published.order_by('start_date', 'start_hour'),
        })
        return context


class EventView(TemplateView):
    template_name = 'page_event.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'item': get_object_or_404(Event, pk=self.kwargs['target']),
        })
        return context


class ContactView(PageMixin, FormView):
    template_name = 'page_contact.html'
    form_class = ContactForm

    def form_valid(self, form):
        body = '''Envoyé le %(date)s
Envoyé par : %(name)s, %(email)s
Pays : %(country)s
Message :
%(message)s
''' % {
            'date': datetime.now(),
            'name': form.cleaned_data['name'],
            'email': form.cleaned_data['email'],
            'country': form.cleaned_data['country'],
            'message': form.cleaned_data['message'],
        }
        send_mail(
            "[cts-pro.org] Message du formulaire contact",
            body,
            settings.SERVER_EMAIL,
            settings.CONTACT_FORM_RECIPIENTS,
        )
        messages.success(self.request, "Merci pour l'envoi de votre message !")
        return HttpResponseRedirect(reverse('home'))
