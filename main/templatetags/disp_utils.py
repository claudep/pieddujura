from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(is_safe=True)
def event_date(event):
    dt = event.date_long(hour=True)
    return mark_safe((dt[0].upper() + dt[1:]).replace(" au ", "<br> au "))


@register.simple_tag(takes_context=True)
def show_content(context, page):
    ev = context['next_event']
    if ev:
        content = page.content.replace(
            '{{ next_event }}',
            '<div class="event-summary"><a href="{}">{}</a> {}</div>'.format(
                ev.get_absolute_url(), ev.date_long(), str(ev)
            )
        )
    else:
        content = page.content.replace('{{ next_event }}', '')
    return mark_safe(content)
