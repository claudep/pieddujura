import re

from django import forms
from django.conf import settings
from django.contrib.contenttypes.forms import generic_inlineformset_factory

from captcha.fields import CaptchaField
from django_summernote.widgets import SummernoteWidget

from .models import Attachment, Contact, Page

YOUTUBE_IFRAME = r'''
    <iframe id="player" type="text/html" width="640" height="360"
    src="http://www.youtube.com/embed/\1?enablejsapi=1&origin=http://%s"
    frameborder="0"></iframe>''' % settings.ALLOWED_HOSTS[0]


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = ['title', 'menu_title', 'content']
        widgets = {
            'content': SummernoteWidget(),
        }

    def __init__(self, *args, data=None, files=None, **kwargs):
        super().__init__(*args, data=data, files=files, **kwargs)
        AttachmentFormSet = generic_inlineformset_factory(model=Attachment, extra=1)
        self.formset = AttachmentFormSet(instance=self.instance, data=data, files=files)

    def clean_content(self):
        content = self.cleaned_data['content']
        content = re.sub(r'https://www\.youtube\.com/watch\?v=([_\w]*)', YOUTUBE_IFRAME, content)
        return content

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        self.formset.save(**kwargs)
        return instance


class ContactForm(forms.Form):
    name = forms.CharField(max_length=200, label="Nom et prénom")
    email = forms.EmailField(label="Courriel")
    country = forms.CharField(max_length=100, label="Pays")
    message = forms.CharField(max_length=2000, label="Votre message", widget=forms.Textarea)
    target = forms.ModelChoiceField(label="Je désire contacter", queryset=Contact.objects.all())
    captcha = CaptchaField(help_text="Écrivez les lettres que vous voyez dans l'image")
