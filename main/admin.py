from datetime import date

from django.contrib import admin
from django.db import models

from mptt.admin import DraggableMPTTAdmin
from django_summernote.widgets import SummernoteWidget

from .models import Attachment, Contact, Event, Page


@admin.register(Page)
class PageAdmin(DraggableMPTTAdmin):
    list_display = ['tree_actions', 'indented_title', 'slug', 'title', 'weight']
    list_display_links = ['indented_title']
    search_fields = ['title', 'content']


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'start_hour', 'is_published']
    date_hierarchy = 'start_date'
    search_fields = ['title', 'description']
    exclude = ('chapeau',)
    formfield_overrides = {
        models.TextField: {'widget': SummernoteWidget}
    }

    class Media:
        css = {
            "all": ("css/event.css",)
        }

    def is_published(self, obj):
        return obj.pub_date <= date.today()
    is_published.boolean = True
    is_published.short_description = "Publié ?"


admin.site.register(Attachment)
admin.site.register(Contact)
